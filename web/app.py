from flask import Flask
from flask import send_file
from flask import render_template
import os

app = Flask(__name__)

WEBROOT = "http"

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def main_logic(path):
  path = "/"+path
  fullPath = WEBROOT+path
  string_ok = True
  split_url = path.split("/")
  for i in range(len(split_url)):
    print("["+split_url[i]+"]")
    if (i>0 and len(split_url[i]) < 1):
      string_ok = False
    if (split_url[i][0:2] == ".."):
      string_ok = False
    elif (split_url[i][0:1] == "~"):
      string_ok = False
  if ("//" in path):
    print(path)
    string_ok = False

  if (string_ok):
    if os.path.exists(fullPath):
      return send_file(WEBROOT+path), 200
    else:
      return render_template("404.html",message="File not found!"), 404
  else:
    return render_template("403.html",message="File is forbidden!"), 403

if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0')
