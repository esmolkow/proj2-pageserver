# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### Features ###

  * A tiny Python webserver.
  * Returns a page if a page exists in `DOCROOT`.
  * Returns 404 if page doesn't exist.
  * Returns 403 if the request path begins with `//`,`/~`, or `/..` at any point.

### What do I need?  Where will it work? ###

* The only tool needed is docker.

### Usage ###

* Clone the repo.

* Run `docker build -t uocis-flask-demo .` from the `web` directory.

* Next, run `docker run -d -p 5000:5000 uocis-flask-demo`.